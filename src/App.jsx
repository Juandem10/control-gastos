import { useState, useEffect } from 'react'


import Header from './components/Header'
import ListadoGastos from './components/ListadoGastos'
import ModalNuevoGasto from './components/ModalNuevoGasto'
import Filtros from './components/Filtros'


import { generarId } from './helpers'



import IconoNuevoGasto from './img/nuevo-gasto.svg'

function App() {

  const [presupuesto, setPresupuesto] = useState(Number(localStorage.getItem('presupuesto')) ?? 0) //Intentamos jalar el presupuesto del localstorage sino inicia con 0
  const [isValidPresupuesto, setIsValidPresupuesto] = useState(false)

  const [modal, setModal] = useState(false)
  const [animarModal, setAnimarModal] = useState(false)

  const [gastos, setGastos] = useState(localStorage.getItem('gastos') ? JSON.parse(localStorage.getItem('gastos')) : []) //Intentamos jalar si hay gastos en el localstorage sino lo iniciamos con arreglo vacio

  const [gastoEditar, setGastoEditar] = useState({})

  const [filtro, setFiltro] = useState('')
  const [gastosFiltrados, setGastosFiltrados] = useState([])

  //Effect para abrir el modal cada vez que se realiza el swipe de editar en un gasto
  useEffect(()=>{
    if(Object.keys(gastoEditar).length > 0){
      setModal(true)
      setTimeout(()=>{
        setAnimarModal(true)
      }, 500)
    }
  }, [gastoEditar])


  //Effect para guardar en el local storage cada vez que se actualiza el presupuesto
  useEffect(()=>{
    localStorage.setItem('presupuesto', presupuesto ?? 0)
  }, [presupuesto])

  //Effect para ejecutar cada vez que se realizan cambios en gastos
  useEffect(()=>{
    localStorage.setItem('gastos', JSON.stringify(gastos) ?? [])
  }, [gastos])


  useEffect(()=>{
    if(filtro){
      //Filtrar gastos por categoria
      const gastosFiltrados = gastos.filter(gasto => gasto.categoria === filtro)
      setGastosFiltrados(gastosFiltrados)
      return
    }

    setGastosFiltrados(gastos)

  }, [filtro])

  //Effect para confirmar que lo que esta en el localstorage es valido
  useEffect(()=>{
    const presupuestoLS = Number(localStorage.getItem('presupuesto')) ?? 0
    if(presupuestoLS > 0){
      setIsValidPresupuesto(true)
    }
  }, [])

  const handleNuevoGasto = () => {
    setModal(true)
    setGastoEditar({}) //Reiniciamos el estado de gasto editar para evitar que al presionar el boton de "nuevo" lo rellene con algun dato que haya quedado al editar
    setTimeout(()=>{
      setAnimarModal(true)
    }, 500)
  }

  const guardarGasto = (gasto) => {
    if(gasto.id){
      //Editar gasto
      const gastosActualizados = gastos.map( gastoState => gastoState.id === gasto.id ? gasto : gastoState) //Recorremos cada gasto y colocamos el editado donde cumpla la condicion de igualar su id
      setGastos(gastosActualizados) //Actualizamos en el state con el nuevo array que nos da el map despues de reemplazar el que modifico
      setGastoEditar({})
    }else{
      //Nuevo gasto
      gasto.id = generarId()
      gasto.fecha = Date.now()
      setGastos([...gastos, gasto])
    }

    setAnimarModal(false)
    setTimeout(() =>{
        setModal(false)
    }, 500)
  }

  const eliminarGasto = id => {
    const gastosActualizados = gastos.filter( gasto  => gasto.id !== id) //Filtramos para traer todos los que no sean el que queremos elminar en un array nuevo
    setGastos(gastosActualizados) //Definimos en el state el nuevo listado de gastos
  }

  return (
    <div className={modal ? 'fijar' : ''}>
      <Header presupuesto={presupuesto} 
        setPresupuesto={setPresupuesto}
        isValidPresupuesto = {isValidPresupuesto}
        setIsValidPresupuesto = {setIsValidPresupuesto}
        gastos = {gastos}
        setGastos = {setGastos}
      />

      {isValidPresupuesto && (
        <>
          <main>
            <Filtros filtro={filtro} setFiltro={setFiltro} />
            <ListadoGastos gastos={gastos} setGastoEditar={setGastoEditar} eliminarGasto={eliminarGasto} filtro={filtro} gastosFiltrados={gastosFiltrados}/>
          </main>
          <div className='nuevo-gasto'>
            <img src={IconoNuevoGasto} 
              alt='Icono nuevo gasto' 
              onClick={handleNuevoGasto}
            />
          </div>
        </>
        
      )}
      
      {modal && <ModalNuevoGasto setModal={setModal} 
        animarModal={animarModal}
        setAnimarModal = {setAnimarModal}
        guardarGasto = {guardarGasto}
        gastoEditar = {gastoEditar}
        setGastoEditar = {setGastoEditar}
      />}

    </div>
    
  )
}

export default App
